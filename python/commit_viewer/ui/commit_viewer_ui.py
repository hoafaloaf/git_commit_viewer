# -*- coding: utf-8 -*-

# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(812, 671)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(4)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_4 = QtGui.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_4.setFont(font)
        self.label_4.setTextFormat(QtCore.Qt.RichText)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout.addWidget(self.label_4)
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setSpacing(9)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QtCore.QSize(75, 0))
        self.label.setMaximumSize(QtCore.QSize(75, 16777215))
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.pathLineEdit = QtGui.QLineEdit(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pathLineEdit.sizePolicy().hasHeightForWidth())
        self.pathLineEdit.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.pathLineEdit.setFont(font)
        self.pathLineEdit.setObjectName(_fromUtf8("pathLineEdit"))
        self.horizontalLayout.addWidget(self.pathLineEdit)
        self.browserButton = QtGui.QPushButton(self.centralwidget)
        self.browserButton.setText(_fromUtf8(""))
        self.browserButton.setIconSize(QtCore.QSize(16, 16))
        self.browserButton.setObjectName(_fromUtf8("browserButton"))
        self.horizontalLayout.addWidget(self.browserButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(9)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setMinimumSize(QtCore.QSize(75, 0))
        self.label_3.setMaximumSize(QtCore.QSize(75, 16777215))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.filterLineEdit = FilterLineEdit(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.filterLineEdit.sizePolicy().hasHeightForWidth())
        self.filterLineEdit.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.filterLineEdit.setFont(font)
        self.filterLineEdit.setObjectName(_fromUtf8("filterLineEdit"))
        self.horizontalLayout_2.addWidget(self.filterLineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.commitTree = QtGui.QTreeView(self.centralwidget)
        self.commitTree.setMinimumSize(QtCore.QSize(800, 0))
        self.commitTree.setRootIsDecorated(True)
        self.commitTree.setObjectName(_fromUtf8("commitTree"))
        self.verticalLayout.addWidget(self.commitTree)
        self.frame = QtGui.QFrame(self.centralwidget)
        self.frame.setFrameShape(QtGui.QFrame.Box)
        self.frame.setFrameShadow(QtGui.QFrame.Plain)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.gridLayout = QtGui.QGridLayout(self.frame)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.cancelButton = QtGui.QPushButton(self.frame)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.cancelButton.setFont(font)
        self.cancelButton.setObjectName(_fromUtf8("cancelButton"))
        self.gridLayout.addWidget(self.cancelButton, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.frame)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 812, 27))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.dateMenu = QtGui.QMenu(self.menubar)
        self.dateMenu.setObjectName(_fromUtf8("dateMenu"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.refreshAction = QtGui.QAction(MainWindow)
        self.refreshAction.setObjectName(_fromUtf8("refreshAction"))
        self.clearAction = QtGui.QAction(MainWindow)
        self.clearAction.setObjectName(_fromUtf8("clearAction"))
        self.calendarDateAction = QtGui.QAction(MainWindow)
        self.calendarDateAction.setCheckable(True)
        self.calendarDateAction.setObjectName(_fromUtf8("calendarDateAction"))
        self.relativeDateAction = QtGui.QAction(MainWindow)
        self.relativeDateAction.setCheckable(True)
        self.relativeDateAction.setObjectName(_fromUtf8("relativeDateAction"))
        self.epochDateAction = QtGui.QAction(MainWindow)
        self.epochDateAction.setCheckable(True)
        self.epochDateAction.setObjectName(_fromUtf8("epochDateAction"))
        self.dateMenu.addAction(self.calendarDateAction)
        self.dateMenu.addAction(self.relativeDateAction)
        self.dateMenu.addAction(self.epochDateAction)
        self.menubar.addAction(self.dateMenu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.label_4.setText(_translate("MainWindow", "Git Commit View-O-Matic 3000", None))
        self.label.setText(_translate("MainWindow", "Git Repository", None))
        self.label_3.setText(_translate("MainWindow", "Filter by Date", None))
        self.cancelButton.setText(_translate("MainWindow", "Close", None))
        self.dateMenu.setTitle(_translate("MainWindow", "Date Options", None))
        self.refreshAction.setText(_translate("MainWindow", "Refresh", None))
        self.clearAction.setText(_translate("MainWindow", "Clear", None))
        self.calendarDateAction.setText(_translate("MainWindow", "calendar", None))
        self.relativeDateAction.setText(_translate("MainWindow", "relative", None))
        self.epochDateAction.setText(_translate("MainWindow", "epoch", None))

from commit_viewer.widgets import FilterLineEdit
import commit_viewer_rc
