"""Custom widgets use by the CommitViewerWindow."""

import re

from PyQt4 import QtCore, QtGui

SAFE_WORDS = ["years", "months", "weeks", "days", "hours", "minutes", "ago"]


class FilterValidator(QtGui.QValidator):
    """Validator used by the FilterLineEdit class."""

    text_validity = QtCore.pyqtSignal(int, name="validity")

    def __init__(self, parent):
        """Initialize the instance."""
        super(FilterValidator, self).__init__(parent)

    def validate(self, input, pos, emit=True):
        """Reimplementation of QValidator method."""
        valid = self.Acceptable
        input = str(input).lower().strip()

        # This condition would match all hashes, so we're inverting it to look
        # for relative dates.
        if not re.match("[0-9][a-f]+$", input):
            bits = input.split()
            while bits:
                bit = bits.pop(0)

                # Matching numbers ...
                if re.match("\d+", bit):
                    continue

                # ... and words.
                valid = self.Invalid
                for word in SAFE_WORDS:
                    if bit in word:
                        valid = self.Intermediate
                        break

                if not valid:
                    break

        if emit:
            self.text_validity.emit(valid)

        return (valid, pos)


class FilterLineEdit(QtGui.QLineEdit):
    """Subclass of QLineEdit with QCompleter shortcuts."""

    final_text = QtCore.pyqtSignal(str)
    valid_text = QtCore.pyqtSignal(int)

    def __init__(self, *args, **kwargs):
        """Initialize the instance."""
        super(FilterLineEdit, self).__init__(*args, **kwargs)

        font = QtGui.QFont()
        font.setPointSize(11)
        self.setFont(font)

        self._items = set()
        self._output_text = ""
        self._valid = True

        self.editingFinished.connect(self.check_validity)
        self.textChanged.connect(self.check_validity)

        validator = FilterValidator(self)
        validator.text_validity.connect(self.store_text)
        self.setValidator(validator)

    def update_completer(self, items=None):
        """Update the associated QCompleter with the supplied items."""
        self._items = set(items or [])
        self._items.add("")

        completer = QtGui.QCompleter(list(self._items), self)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.setCompleter(completer)

    def setText(self, val):
        """Reimplementation of QLineEdit method (added validity check)."""
        super(FilterLineEdit, self).setText(val)
        self.check_validity()

    def check_validity(self, val=None):
        """Check the validity of the current text."""
        validator = self.validator()

        if val is None:
            validity = QtGui.QValidator.Acceptable
            if validator:
                validity = validator.validate(self.text(), 0, emit=False)[0]

            self._valid = validity

        self.valid_text.emit(self._valid)

    def store_text(self, validity):
        """Store current text if it's entirely valid."""
        text = str(self.text())

        if text in self._items and text != self._output_text:
            self._output_text = text
            self.final_text.emit(text)

        self._validity = validity
        self.check_validity()

    def focusOutEvent(self, event):
        """Reimplementation of QLineEdit method."""
        text = str(self.text())
        if text in self._items:
            if text != self._output_text:
                self._output_text = text
                self.final_text.emit(text)
        else:
            self.setText(self._output_text)

        super(FilterLineEdit, self).focusOutEvent(event)
