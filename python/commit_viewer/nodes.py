"""Nodes referenced by the models used by the CommitViewerWindow class."""

import os
import shlex
import subprocess

__all__ = ["CommitNode", "RootNode"]

GIT_QUERY_LOG_FULL_CMD = 'git show -q {commit}'

###############################################################################
# class: RootNode


class RootNode(object):
    """Base node type for information in a CommitViewerWindow instance."""

    _TYPE = "root"

    def __init__(self, name="__NONE__"):
        """Initialize the instance."""
        self.children = list()
        self.info = dict()
        self.name = name
        self.repository = None
        self.parent = None

        # We'll use this to
        # already been added as a child!
        self.node_dict = dict()

    def __repr__(self):
        """Return a pretty representation of the class instance."""
        opts = dict(klass=type(self).__name__, name=self.name)

        blurb = '{klass}("{name}")'
        return blurb.format(**opts)

    @property
    def node_type(self):
        """Return the type of the node instance."""
        return self._TYPE

    def add_child(self, child):
        """Add the specified node as a child."""
        name = child.name

        if name not in self.node_dict:
            self.children.append(child)
            self.node_dict[name] = child
            child.parent = self
            child.set_repository(self.repository)

    def add_children(self, children):
        """Add the specified nodes as children."""
        map(self.add_child, children)

    def child(self, row):
        """Return the child for the specified row."""
        return self.children[row]

    def child_count(self):
        """Return the number of children associates with this node."""
        return len(self.children)

    def insert_child(self, position, child):
        """Insert a node as a child at a particular position."""
        count = self.child_count()
        if count and 0 <= position < count:
            return False

        self.children.insert(position, child)
        child.parent = self
        return True

    def remove_child(self, position):
        """Remove the child at the specified position."""
        if not 0 <= position < self.child_count():
            return False

        node = self.children.pop(position)
        node.parent = None
        self.node_dict.pop(node.name, None)

        return True

    def row(self):
        """Return the row of the current node."""
        if self.parent is not None:
            return self.parent.children.index(self)

    def set_repository(self, repo):
        """Set the repository for node and all of its children."""
        self.repository = repo
        for child in self.children:
            child.set_repository(repo)

    def log(self, tabLevel=-1):
        """Return a nifty tree view of the node and its children."""
        output = ""
        tabLevel += 1

        for ii in xrange(tabLevel):
            output += "    "

        output += "|-- %s\n" % repr(self)

        for child in self.children:
            output += child.log(tabLevel)

        tabLevel -= 1
        output += "\n"

        return output

    @staticmethod
    def run_command_simple(cmd):
        """Run the specified command without parsing output."""
        opts = dict(stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        stdout, stderr = subprocess.Popen(cmd, **opts).communicate()

        return stdout, stderr

###############################################################################
# class: CommitNode


class CommitNode(RootNode):
    """Node representing commits in a CommitViewerWindow instance."""

    _TYPE = "commit"

    def __init__(self, commit, info):
        """Initialize the instance."""
        super(CommitNode, self).__init__(commit[:8])

        self.commit = commit
        self.info = info

    def __repr__(self):
        """Return a pretty representation of the class instance."""
        opts = dict(author=self.info["author"],
                    commit=self.commit[:8],
                    date=self.info["date-relative"],
                    klass=type(self).__name__)

        blurb = '{klass}("{commit}", author="{author}", date="{date}")'
        return blurb.format(**opts)

    def add_info_node(self):
        """Add a child InfoNode instance. Only one child is allowed."""
        if not self.child_count():
            node = InfoNode(self.info["commit"])
            self.add_child(node)

###############################################################################
# class: InfoNode


class InfoNode(RootNode):
    """Node representing commit information in CommitViewerWindow instances."""

    _TYPE = "info"

    def __init__(self, commit):
        """Initialize the instance."""
        super(InfoNode, self).__init__("%s_info" % commit)

        self.commit = commit
        self.info = dict()

        self.is_populated = False
        self.commit_log = None

    '''
    def __repr__(self):
        """Return a pretty representation of the class instance."""
        opts = dict(author=self.info["author"],
                    commit=self.commit[:8],
                    date=self.info["date-relative"],
                    klass=type(self).__name__)

        blurb = '{klass}("{commit}", author="{author}", date="{date}")'
        return blurb.format(**opts)
    '''

    def populate_log(self):
        """Get full commit data, add it to display under clicked node."""
        if self.is_populated:
            return self.commit_log

        os.chdir(self.repository)

        cmd = shlex.split(GIT_QUERY_LOG_FULL_CMD.format(commit=self.commit))
        out, err = self.run_command_simple(cmd)

        if out:
            self.commit_log = out
        else:
            print err
            self.commit_log = ""

        self.is_populated = True
        return self.commit_log
