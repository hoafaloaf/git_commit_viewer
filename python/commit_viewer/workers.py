"""Workers used by the CommitViewerWindow class."""

import os
import re
import shlex
import subprocess

from PyQt4 import QtCore

__all__ = ["CommitLogWorker"]

# These are the commands used below to query our git repositories.
# It's a bit sloppy and something I'll fix in the next round of
# development.

GIT_LOG_OPTIONS = [
    ("commit", "%H"), ("author", "%an <%ae>"), ("date-epoch", "%at"),
    ("date-relative", "%ar"), ("date", "%ad"), ("message", "%s")
]

log_opts = ["[[[%s]]]%s" % x for x in GIT_LOG_OPTIONS]

GIT_QUERY_LOG_CMD = 'git log --pretty=format:"%s"' % "".join(log_opts)
GIT_COUNT_COMMITS_CMD = "git rev-list --all --count"

###############################################################################
# class: GenericWorker


class GenericWorker(QtCore.QObject):
    """Generic worker object for processes used by CommitViewerWindow."""

    start = QtCore.pyqtSignal(str)
    finished = QtCore.pyqtSignal()

    def __init__(self):
        """Initialize the instance."""
        super(GenericWorker, self).__init__()

        self.start.connect(self.run)

        self.repository = None
        self.stopped = True
        self.working = False

    @QtCore.pyqtSlot(str)
    def run(self, repo):
        """Initiate processing of the worker."""
        self.repository = repo
        self.stopped = False
        self.working = True

        # Signal that the worker is done processing.
        self.stop()

    @QtCore.pyqtSlot()
    def stop(self):
        """Stop processing of the worker."""
        self.stopped = True
        self.working = False

        self.finished.emit()

    def run_command_simple(self, cmd):
        """Run the specified command without parsing output."""
        opts = dict(stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        stdout, stderr = subprocess.Popen(cmd, **opts).communicate()

        return stdout, stderr

###############################################################################
# class: CommitLogWorker


class CommitLogWorker(GenericWorker):
    """Worker object used by CommitViewerWindow for git repository queries."""

    # These will be used to communicate commit-related information back to the
    # main interface.
    commit_count_changed = QtCore.pyqtSignal(int, str)
    commit_data_updated = QtCore.pyqtSignal(dict, str)
    commit_percentage_changed = QtCore.pyqtSignal(int, str)

    def __init__(self):
        """Initialize the instance."""
        super(CommitLogWorker, self).__init__()

        # These are used primarily to update the progress bar in the main
        # window of the application.
        self.commit_count = 0
        self.commit_index = 0
        self.commit_percentage = 0

        # We'll store all relevant commit info here in chunks, reporting it to
        # the main application at measured intervals.
        self.commit_data = dict()

    @QtCore.pyqtSlot(str)
    def run(self, repo):
        """Initiate processing of the worker."""
        self.repository = str(repo)
        self.stopped = False
        self.working = True

        # Query the number of commits in the repository. We'll use this to
        # update a progress bar in the main interface.
        self.commit_count = self.count_commits()
        self.commit_index = 0

        if self.stopped or not self.commit_count:
            self.stop()
            return

        # Query all commits in the git repository, periodically emiting updates
        # back to the main interface.
        self.query_log()

        # Signal that the worker is done processing.
        self.stop()

    def count_commits(self):
        """Query the number of commits in the specified git repository."""
        cmd = shlex.split(GIT_COUNT_COMMITS_CMD)

        os.chdir(self.repository)
        out, err = self.run_command_simple(cmd)

        try:
            count = int(out.strip())
        except:
            count = 0
            if err:
                print err

        self.commit_count_changed.emit(count, self.repository)
        return count

    def query_log(self):
        """Query the specified git repository's logs."""
        # Zeroing out the current commit data ...
        self.clear_commit_data()

        cmd = shlex.split(GIT_QUERY_LOG_CMD)
        self.run_command_complex(cmd)

        self.report_commit_data()

    def parse_line(self, line):
        """Parse data reported by run_command_complex method."""
        line = re.sub("[\r\n]", "\n", line.rstrip())

        for bit in line.split("\n"):
            if not bit:
                continue

            self.commit_index += 1
            info = dict()

            # we'll deal with the "message" field separately (as it could
            # contain odd formatting).
            for ii in xrange(len(GIT_LOG_OPTIONS) - 1):
                field = GIT_LOG_OPTIONS[ii][0]
                hh = re.escape("[[[%s]]]" % GIT_LOG_OPTIONS[ii][0])
                tt = re.escape("[[[%s]]]" % GIT_LOG_OPTIONS[ii + 1][0])

                regex = "(?P<head>%s)(?P<value>.*)(?P<tail>%s)" % (hh, tt)
                head, value, tail = re.match(regex, bit).groups()

                info[field] = value

                bit = bit[len(head) + len(value):]

            # Massaging the message data ...
            hh = "[[[%s]]]" % GIT_LOG_OPTIONS[-1][0]
            msg = bit.replace(hh, "")
            msg = re.sub("\s+", " ", msg)

            info["message"] = msg

            if self.commit_index % 100 == 0:
                pct = int(100.0 * self.commit_index / self.commit_count)
                self.commit_percentage = pct
                self.report_commit_data()

                # Clearing out the data for the next batch.
                self.clear_commit_data()

            # Storing data from the current commit ...
            self.commit_data[info["commit"]] = info

    def clear_commit_data(self):
        """Clear cached commit data."""
        self.commit_data = dict()

    def report_commit_data(self):
        """Report commit data back to the main interface."""
        self.commit_data_updated.emit(dict(self.commit_data), self.repository)
        self.commit_percentage_changed.emit(self.commit_percentage,
                                            self.repository)

    def run_command_complex(self, cmd):
        """Run specified command, parse all output."""
        opts = dict(stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        proc = subprocess.Popen(cmd, **opts)

        while True:
            if self.stopped:
                return

            line = proc.stdout.readline()
            if line == '' and proc.poll() is not None:
                break

            if line:
                self.parse_line(line)

        retval = proc.poll()

        return retval
