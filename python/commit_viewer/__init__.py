"""Question 3: The Git Commit Viewer."""

from commit_viewer.commit_viewer_gui import CommitViewerWindow

__all__ = ['launch_commit_viewer']

# We'll use this to store a reference to any CommitViewerWindow instance we
# create.
VIEWER = None


def launch_commit_viewer(dir_name=None):
    """Launch the Git Commit Viewer."""
    global VIEWER

    if isinstance(VIEWER, CommitViewerWindow):
        try:
            VIEWER.close()
        except:
            VIEWER = None

    VIEWER = CommitViewerWindow(dir_name)
    VIEWER.show()
