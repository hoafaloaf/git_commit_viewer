"""The meat of the Git Commit Viewer interface code."""

import os

from PyQt4 import QtCore, QtGui

from commit_viewer.models import (GitCommitItemModel,
                                             GitCommitSortFilterProxyModel)
from commit_viewer.nodes import CommitNode, RootNode
from commit_viewer.ui import commit_viewer_ui
from commit_viewer.workers import CommitLogWorker

# These are used to update the Window's statusBar message.
UPDATE_MSG = "Updating view ..."
COMPLETE_MSG = "Reading complete."
FULL_QUERY_MSG = "Reading log for {commit} ..."
READY_MSG = "Ready."

COLUMN_WIDTHS = [400, 200, 200, 200, 200]


class CommitViewerWindow(QtGui.QMainWindow, commit_viewer_ui.Ui_MainWindow):
    """Git Commit Viewer Interface."""

    def __init__(self, dir_name=None):
        """Initialize the instance."""
        super(CommitViewerWindow, self).__init__()

        self._initialize()
        self.setupUi(self)
        self.setWindowTitle("Git View-O-Matic 3000")

        # Set up the model(s).
        self.setup_models()

        # Connect everything up.
        self.setup_main_interface()

        # Set relevant attributes.
        self.repository = dir_name

    def _initialize(self):
        """Initialize properties and settings for the class."""
        self._repo = None

        self.commit_count = 0
        self.commit_data = dict()
        self.commit_percentage = 0
        self.current_worker = None
        self.progress_bar = None
        self.root = RootNode()
        self.thread_pool = list()

        self.node_model = None
        self.node_proxy = None

        # Throwing the "commit" column in here as we'll use this to hide
        # columns the column in the interface.
        self.date_action_mapping = dict(calendar="date",
                                        commit="commit",
                                        epoch="date-epoch",
                                        relative="date-relative")

    @property
    def repository(self):
        """Return the location of the current repository being browsed."""
        return self._repo

    @repository.setter
    def repository(self, repo):
        self.set_repository(str(repo))

    def setup_main_interface(self):
        """Setup the interface, connect signals/slots."""
        self.cancelButton.clicked.connect(self.close)

        # Could have done this in the .ui file, but it's a lot more flexible
        # (and obvious) to do it in the main code.
        pixmap = QtGui.QPixmap(":/icons/cherub_grumpy.jpg")
        icon = QtGui.QIcon()
        icon.addPixmap(pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)

        pixmap = QtGui.QPixmap(":/icons/ic_folder_open_black_24dp.png")
        icon = QtGui.QIcon()
        icon.addPixmap(pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.browserButton.setIcon(icon)
        self.browserButton.setIconSize(QtCore.QSize(24, 24))
        self.browserButton.clicked.connect(self.launch_file_browser)

        self.progress_bar = QtGui.QProgressBar()
        self.progress_bar.setMinimum(0)
        self.progress_bar.setMaximum(100)

        # Connect date options (default to "calendar")
        self.date_action_group = QtGui.QActionGroup(self)
        self.date_action_group.addAction(self.calendarDateAction)
        self.date_action_group.addAction(self.relativeDateAction)
        self.date_action_group.addAction(self.epochDateAction)
        self.date_action_group.triggered.connect(self.set_date_type)

        self.set_date_type(self.calendarDateAction)

        # Set the column widths in the QTreeView ...
        for column, width in enumerate(COLUMN_WIDTHS):
            self.commitTree.setColumnWidth(column, width)

        self.commitTree.expanded.connect(self.launch_full_log_query)
        self.commitTree.setRootIsDecorated(True)

        # Connect up the QLineEdit so that we can use it for filtering our
        # model.
        self.filterLineEdit.final_text.connect(self.filter_commits)

        # Connect up the QTreeView so that we can create child "info" nodes
        # to report full commit data.

        status_bar = self.statusBar()
        status_bar.addPermanentWidget(self.progress_bar)
        status_bar.messageChanged.connect(self.reset_statusbar)

    def setup_models(self):
        """Set up the models associated with the interface."""
        self.node_model = None
        self.node_proxy = GitCommitSortFilterProxyModel()

        # This will reset the item model for the proxy.
        self.refresh_item_model()

        self.commitTree.setModel(self.node_proxy)
        self.commitTree.setAlternatingRowColors(True)
        self.commitTree.setRootIsDecorated(False)
        self.commitTree.setSortingEnabled(True)

    def refresh_item_model(self):
        """Update the proxy model with new item model."""
        self.node_model = GitCommitItemModel(self.root)
        self.node_proxy.setSourceModel(self.node_model)

    def set_date_type(self, action):
        """Hide all date columns in the TreeView other the one specified."""
        self.date_action_group.blockSignals(True)
        action.setChecked(True)
        self.date_action_group.blockSignals(False)

        show_column = self.date_action_mapping[str(action.text())]
        hide_columns = set(self.date_action_mapping.values())
        hide_columns -= set([show_column])

        columns = self.node_proxy.COLUMNS

        self.commitTree.setColumnHidden(columns.index(show_column), False)
        for column in hide_columns:
            self.commitTree.setColumnHidden(columns.index(column), True)

    def set_repository(self, repo, refresh=True):
        """Set the git repository property."""
        if not repo:
            repo = os.getcwd()

        if not os.path.isdir(repo):
            repo = os.path.dirname(repo)
            if not os.path.isdir(repo):
                repo = os.getcwd()

        msg = "Git repository selected!"
        is_repo = True
        if ".git" not in os.listdir(repo):
            msg = "WARNING: directory selected is not a git repository!"
            is_repo = False

        self.statusBar().showMessage(msg)
        self._repo = repo

        self.pathLineEdit.blockSignals(True)
        self.pathLineEdit.setText(repo)
        self.pathLineEdit.blockSignals(False)

        if is_repo:
            self.stop_git_queries()
            self.launch_repo_query(self.repository)

    def set_commit_count(self, count, repo=None):
        """Setter method for commit_count attribute."""
        # Don't update the commit count unless the reported repository matches
        # the one currently being queried by the interface.
        if repo and repo == self.repository:
            self.commit_count = count

    def set_commit_percentage(self, pct, repo=None):
        """Setter method for commit_count attribute."""
        # Don't update the commit count unless the reported repository matches
        # the one currently being queried by the interface.
        if repo and repo == self.repository:
            self.commit_percentage = pct
            self.progress_bar.setValue(pct)

    def filter_commits(self, filter_date):
        """Adjust filtering on data displayed in the interface."""
        if filter == self.node_proxy.filter_date:
            return

        self.node_proxy.filter_date = filter_date

    def update_commit_data(self, data):
        """Update the commit data displayed by the interface."""
        self.commit_data.update(data)

    def reset_statusbar(self, msg):
        """Reset the status bar when temporary message is removed."""
        if msg == COMPLETE_MSG:
            self.statusBar().showMessage(READY_MSG)
            self.progress_bar.setValue(0)

    def launch_file_browser(self):
        """Launch file browser for git repository selection."""
        dialog = QtGui.QFileDialog(self)
        dialog.setFileMode(dialog.DirectoryOnly)

        dir_name = dialog.getExistingDirectory(self, "Select Directory",
                                               self.repository)

        if dir_name and dir_name != self.repository:
            self.repository = dir_name

    def launch_full_log_query(self, index):
        """Get full commit data, add it to display under clicked node."""
        item = index.child(0, 0)

        # Make sure that the first column is fully expanded for the InfoNode
        # display.
        self.commitTree.setFirstColumnSpanned(0, item, True)

        node = item.data(QtCore.Qt.UserRole).toPyObject()
        if not node.is_populated:
            self.statusBar().showMessage(FULL_QUERY_MSG)
            node.populate_log()
            self.statusBar().showMessage(READY_MSG)

    def launch_repo_query(self, repo):
        """Begin querying specified repository in external thread."""
        self.commit_data.clear()

        # Create the thread where we'll run the git querying worker.
        thread = QtCore.QThread(self)
        thread.start()
        self.thread_pool.append(thread)

        worker = self.current_worker = CommitLogWorker()

        worker.finished.connect(self.finish_repo_query)
        worker.commit_count_changed.connect(self.set_commit_count)
        worker.commit_data_updated.connect(self.update_commit_data)
        worker.commit_percentage_changed.connect(self.set_commit_percentage)

        self.statusBar().showMessage("Reading Git Repository ...")
        worker.moveToThread(thread)
        worker.start.emit(repo)

    def finish_repo_query(self):
        """Update interface to show that current git query is finished."""
        self.statusBar().showMessage(UPDATE_MSG)

        # Remove all of the root node's children!
        del self.root.children[:]

        # Set the repository on the node for reference.
        self.root.set_repository(self.repository)

        # We'll use these to update the completer for the FilterLineEdit.
        completer_terms = set()

        # It's easier to make the "info" nodes first and have their full log
        # information added the first time they're exposed.
        for commit, info in self.commit_data.iteritems():
            commit_node = CommitNode(commit, info)
            commit_node.add_info_node()

            self.root.add_child(commit_node)
            completer_terms.add(info["date-relative"])
            completer_terms.add(info["commit"])

        self.filterLineEdit.update_completer(sorted(completer_terms))

        self.refresh_item_model()
        self.statusBar().showMessage(COMPLETE_MSG, 2000)

    def stop_git_queries(self):
        """Stop processing current git query."""
        if self.current_worker and self.current_worker.working:
            self.current_worker.stop()
            self.current_worker = None

    def closeEvent(self, event):
        """Reimplementation of closeEvent to clean up any threads."""
        self.stop_git_queries()
