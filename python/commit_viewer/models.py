"""Models used by the CommitViewerWindow."""

from PyQt4 import QtCore, QtGui

from commit_viewer.nodes import InfoNode

__all__ = ["GitCommitItemModel", "GitCommitSortFilterProxyModel"]

COLUMNS = ["message", "author", "date", "date-relative", "date-epoch",
           "commit"]
COLUMN_COUNT = len(COLUMNS)

##############################################################################
# class: GitCommitItemModel


class GitCommitItemModel(QtCore.QAbstractItemModel):
    """Item model used to display nodes in the CommitViewerWindow."""

    COLUMNS = COLUMNS
    COLUMN_COUNT = COLUMN_COUNT

    def __init__(self, root, parent=None):
        """Initialize the instance."""
        super(GitCommitItemModel, self).__init__(parent)
        self._initialize()

        self.root = root

    def _initialize(self):
        """Initialize important attributes and settings on the instance."""
        self._root = None

    @property
    def root(self):
        """Return the root node of the model."""
        return self._root

    @root.setter
    def root(self, val):
        self._root = val

    def rowCount(self, parent):
        """Reimplementation of QAbstractItemModel method."""
        if parent.isValid():
            mom = parent.internalPointer()
        else:
            mom = self.root

        return mom.child_count()

    def columnCount(self, parent=QtCore.QModelIndex()):
        """Reimplementation of QAbstractItemModel method."""
        return self.COLUMN_COUNT

    def data(self, index, role):
        """Reimplementation of QAbstractItemModel method."""
        if not index.isValid():
            return QtCore.QVariant()

        node = index.internalPointer()

        if role == QtCore.Qt.UserRole:
            return node

        else:
            return QtCore.QVariant()

    def get_node(self, index):
        """Return the node corresponding to the specified index."""
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self.root

    def headerData(self, section, orientation, role):
        """Reimplementation of QAbstractItemModel method."""
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                title = self.COLUMNS[section]
                return title
            else:
                return ""

    def index(self, row, column, parent=QtCore.QModelIndex()):
        """Reimplementation of QAbstractItemModel method."""
        mom = self.get_node(parent)
        kid = mom.child(row)

        if kid:
            return self.createIndex(row, column, kid)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        """Reimplementation of QAbstractItemModel method."""
        node = self.get_node(index)
        mom = node.parent

        if mom == self.root:
            return QtCore.QModelIndex()

        return self.createIndex(mom.row(), 0, mom)

    def insertRows(self, position, rows, parent=QtCore.QModelIndex()):
        """Reimplementation of QAbstractItemModel method."""
        mom = self.get_node(parent)

        self.beginInsertRows(parent, position, position + rows - 1)

        for row in range(rows):
            name = "%s_%d" % (mom.info["commit"], mom.child_count())
            kid = InfoNode(name)
            success = mom.insert_child(position, kid)

            print "kid:", kid
            print mom.children

        self.endInsertRows()
        return success

    def removeRows(self, position, rows, parent=QtCore.QModelIndex()):
        """Reimplementation of QAbstractItemModel method."""
        mom = self.get_node(parent)
        self.beginRemoveRows(parent, position, position + rows - 1)

        for row in range(rows):
            success = mom.removeChild(position)

        self.endRemoveRows()

        return success

##############################################################################
# class: GitCommitSortFilterProxyModel


class GitCommitSortFilterProxyModel(QtGui.QSortFilterProxyModel):
    """Model used to sort/filter nodes in the CommitViewerWindow."""

    proxySorted = QtCore.pyqtSignal()

    COLUMNS = COLUMNS
    COLUMN_COUNT = COLUMN_COUNT

    def __init__(self, *args, **kwargs):
        """Initialize the instance."""
        super(GitCommitSortFilterProxyModel, self).__init__(*args, **kwargs)

        self._filter = ""

        self.setDynamicSortFilter(True)

    @property
    def nodes(self):
        """Return the list of nodes managed by the interface."""
        model = self.sourceModel()

        nodes = list()
        if model:
            nodes = model.root.children

        return nodes

    @property
    def filter_date(self):
        """Filter (relative date) used to limit commit data display."""
        return self._filter

    @filter_date.setter
    def filter_date(self, val):
        val = str(val or "")
        if val == self._filter:
            return

        self._filter = val
        self.invalidateFilter()

    def data(self, index, role):
        """Reimplementation of QSortFilterProxyModel method."""
        if not index.isValid():
            return None

        node = self.mapToSource(index).data(QtCore.Qt.UserRole).toPyObject()
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if node.node_type == "info" and column == 0:
                data = node.commit_log
            else:
                data = node.info.get(self.COLUMNS[column], "")

            return data

        elif role == QtCore.Qt.FontRole:
            font = QtGui.QFont()
            font.setPointSize(10)

            if node.node_type == "info":
                font.setFamily("Courier New")

            return font

        elif role == QtCore.Qt.UserRole:
            return QtCore.QVariant(node)

        elif role == QtCore.Qt.SizeHintRole:
            if node.node_type == "info":
                return QtCore.QVariant()
            return QtCore.QSize(20, 20)

        else:
            return QtCore.QVariant()

    def filterAcceptsRow(self, row, parent):
        """Reimplementation of QSortFilterProxyModel method."""
        index = self.sourceModel().index(row, 0, parent)
        toggle = True

        node = index.internalPointer()

        if self.filter_date:
            if node.node_type == "info":
                relative_date = node.parent.info["date-relative"]
                commit = node.parent.info["commit"]
            else:
                relative_date = node.info["date-relative"]
                commit = node.info["commit"]

            toggle = self.filter_date == relative_date
            if not toggle:
                toggle = self.filter_date == commit

        return toggle

    def flags(self, index):
        """Reimplementation of QSortFilterProxyModel method."""
        ff = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        return ff

    def setSourceModel(self, model):
        """Reimplementation of QSortFilterProxyModel method."""
        super(GitCommitSortFilterProxyModel, self).setSourceModel(model)
        self.sort(self.COLUMNS.index("date-epoch"), QtCore.Qt.DescendingOrder)

    def sort(self, row, order):
        """Reimplementation of QSortFilterProxyModel method."""
        super(GitCommitSortFilterProxyModel, self).sort(row, order)
        self.proxySorted.emit()

    def lessThan(self, left, right):
        """Reimplementation of QSortFilterProxyModel method."""
        column = left.column()

        left = left.internalPointer()
        right = right.internalPointer()

        # Allow sorting by "epoch" (not alphabetic) date.
        if self.COLUMNS[column].startswith("date"):
            lname = left.info.get("date-epoch")
            rname = right.info.get("date-epoch")

        else:
            lname = left.info.get(self.COLUMNS[column])
            rname = right.info.get(self.COLUMNS[column])

        if left.node_type == "root":
            if right.node_type == "root":
                retval = lname < rname
            else:
                retval = True

        elif right.node_type == "root":
            retval = False

        else:
            # Add additional sorting by "epoch" date if column is "author."
            if lname == rname and self.COLUMNS[column] == "author":
                lname = left.info.get("date-epoch")
                rname = right.info.get("date-epoch")

            retval = lname < rname

        return retval
