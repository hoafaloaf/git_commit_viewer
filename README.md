# Git Commit Viewer

In this package you'll find a PyQt-based coding sample that I've written (mostly
for fun) over the a few days.

## Where Does the Code Live?

Take a look in the *python/* directory!

## How Do I Test the Code?

You'll be able to test all of the code using the script located in the *bin/*
directory.

In a Unix-like environment you'll just be able to just run it as so:

```bash
python bin/commit_viewer [optional path to git repository]
bin/commit_viewer [optional path to git repository] # It's executable with a "shebang"
```

**NOTE:** The script does a bit of path-parsing magic (based upon the relative
path to the module in this packages current structure) to determine what it
needs to add to *PYTHONPATH*.

## What I Was Trying ...

Write an application for browsing git commit history that ...

* Had a UI to navigate to a directory
* A TreeView to load each comment (model/view FTW)
* Add for some searching capabilities (I hard-coded it to search for relative
  commit date and/or commit hash).
* Make it multi-threaded to allow the UI to remain responsive while executing
  any git commands in the background.

## Notes on the Code

* Interface: `python/commit_viewer/commit_viewer_gui.py`
    * Source .ui: `python/commit_viewer/ui/commit_viewer.ui`
* Workers (used for threaded processes): `python/commit_viewer/workers.py`
* Models: `python/commit_viewer/models.py`
* Nodes (referenced by the models): `python/commit_viewer/nodes.py`
* Widgets (reference by the .ui file): `python/commit_viewer/widgets.py`

If run without any arguments in a git repository (say, this one here),
`commit_viewer` will immediately try to read the repository in a background
thread. You'll still be able to use the interface, though, to navigate via the
browser button in the upper right-hand corner of the interface to another
repository should you so desire.

Alternately, you may specify a path to a the git repository of your choice, a la

```bash
python bin/commit_viewer /path/to/some/other/repository
```

Here's how I approached coding up my idea:

1. Created .ui design (with a few icons) using designer. Coupled with the use of
   `pyuic4`, this allowed rapid prototyping of a quickly instantiating
   interface that was easy to pull into the main window class
   (`CommitViewerWindow`, located in the `commit_viewer.py` file).
2. For flexibility, I used a combination of models.
    1. The `GitCommitItemModel` (subclassed from `QAbstractItemModel`) utilizes
       3 related node types (`RootNode`, `CommitNode`, and `ItemNode`, all
       created from scratch). I used this solely to manage the data gleaned from
       any given git repository.
    2. The `GitCommitSortFilterProxyModel` (subclassed from
       `QSortFilterProxyModel`) to handle all sorting, filtering, and displaying
       of the commit data.
3. The `RootNode` is instantiated at the creation of the QMainWindow.
   `CommitNode` instances are created as needed once the threaded git queries
   run in the `CommitLogWorker` reports its findings back to the main thread.
4. `InfoNodes` attached to each `CommitNode` initially as a collapsed child
   node lazy load full commit data when first displayed -- double click on any
   `CommitNode` instance to see how they work. *NOTE:* Commit data might (this
   was inconsistent in my testing) show up on one line when the first node is
   displayed; collapsing/expanding the node display one more time usually fixes
   this behavior.
5. Model/View sorting by **relative date** and **commit hash** is possible via
   the *Filter* QLineEdit. I chose to cache out all relative dates and commit
   hashes into a completer for the QLineEdit.

## Conclusion

The `commit_viewer` interface works as a Milestone 1 type of application: it
(generally) seems to meet the initial requirements, but could use a lot of
improvements to make it completely useful.

It was fun writing it, to be honest. :D

Ideally I'd like to add in some delegate stuff to make it as fast as possible,
but that'll have to wait until I have a bit more free time.
